import classes from './Coin.css';

const Coin = ({ small, click }) => {

  const smallClass = small ? classes.Small : null;
  const smallIconClass = small ? classes.SmallIcon : null;

  return <div className={classes.Coin} className={[classes.Coin, smallClass].join(' ')} onClick={click}>
    <img src="/assets/coins/cake.svg" className={[classes.CoinIcon, smallIconClass].join(' ')} width={32} height={32} fill="white" />
  </div>;
};

export default Coin;
