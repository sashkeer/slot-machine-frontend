import classes from './Button.css';

const button = ({ children, click, disabled, small }) => {

  const disabledClass = disabled ? classes.Disabled : null;

  const smallClass = small ? classes.Small : null;

  return <div className={[classes.Button, disabledClass, smallClass].join(' ')} onClick={click}>
    { children }
  </div>;
};

export default button;