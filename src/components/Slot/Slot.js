import { useEffect, useState } from 'react';

import classes from './Slot.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { handleSlotDone, handleSlotStarted } from '../../actions/SlotActions';

function easeInOutExpo(x) {
  return x === 0
    ? 0
    : x === 1
      ? 1
      : x < 0.5 ? (2)**(20 * x - 10) / 2
        : (2 - (2)**(-20 * x + 10)) / 2;
}

function angleEasingAnimation(start, end, runningTimer) {
  return start + easeInOutExpo(runningTimer / (end - start)) * end;
}
const Slot = ({ newAngle, counter, slots, highlight, handleSlotDoneAction, handleSlotStartedAction }) => {

  const highlightClass = highlight ? classes.Highlight : null;

  const step = 60;

  const [timer, setTimerState] = useState(0);
  const [interval, setIntervalState] = useState(0);
  const [angle, setAngleState] = useState(0);
  const [nextAngle, setNextAngleState] = useState(0);
  const [lastAngle, setLastAngleState] = useState(0);

  useEffect(() => {
    handleSlotStartedAction();
    setTimerState(lastAngle);
    setNextAngleState(newAngle + lastAngle);
  }, [newAngle, counter]);

  useEffect(() => {
    setIntervalState(setInterval(() => {
      setTimerState((currentTimer) => currentTimer + step);
    }, 100));
    return () => {
      clearInterval(interval);
    }
  }, [nextAngle]);

  useEffect(() => {
    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    if (timer > nextAngle) {
      clearInterval(interval);
      setAngleState(nextAngle);
      setLastAngleState(nextAngle);
      handleSlotDoneAction();
    } else {
      setAngleState(angleEasingAnimation(0, nextAngle - lastAngle, timer - lastAngle) + lastAngle);
    }
  }, [timer]);


  useEffect(() => {
    setRotationState({
      'transform': `rotateX(${angle || 0}deg)`
    });
  }, [angle]);

  const [rotation, setRotationState] = useState({
    'transform': `rotateX(${angle || 0}deg)`
  });

  const posters = slots.slots.map((value, index, arr) => {

    const step = (360 / arr.length) >> 0;

    const style = {
      WebkitTransform: `rotateX(${step * index}deg) translate3d(-50%, -50%, 210px)`,
    };

    return <div
      key={value.id}
      className={classes.Picture}
      style={style}>
      <div className={classes.PictureInner}>
        {/*<span className={classes.Number}>{value.id}</span>*/}
        <img src={value.icon}
             className={classes.Poster} />
      </div>
    </div>;
  });

  return <div className={[classes.Slot, highlightClass].join(' ')}>
    <div className={classes.Rotate}>
      <div className={classes.Ring} style={rotation}>
        { posters }
      </div>
    </div>
  </div>;
};

Slot.propTypes = {
  newAngle: PropTypes.number,
  counter: PropTypes.number,
};

const mapStateToProps = (store) => {
  return {
    slots: store.slots,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleSlotStartedAction: () => dispatch(handleSlotStarted()),
    handleSlotDoneAction: () => dispatch(handleSlotDone()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Slot);
