import classes from './Modal.css';
import { connect } from 'react-redux';

const Modal = ({ children, modal }) => {
  return modal.isOpen
    ? <div className={classes.ModalWrapper}>
      <div className={classes.Modal}>
        { children }
      </div>
    </div>
    : null;
};

const mapStateToProps = (store) => {
  return {
    modal: store.modal,
  }
};

export default connect(
  mapStateToProps,
)(Modal)
