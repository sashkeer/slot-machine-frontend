import classes from './Rules.css';
import { connect } from 'react-redux';
import { FaCoins, FaInfoCircle } from "react-icons/fa";
import Coin from '../Coin/Coin';

const Rules = ({ slots }) => {
  return <div className={classes.MainRules}>

    <h1>Добро пожаловать!</h1>

    <p>Перед вами лотерея. <b>Раз в месяц</b> я разыгрываю <b>несколько десертов</b> и готова сделать их абсолютно <b>бесплатно</b>, для вас!</p>

    <p>Чтобы победить, соберите в ряд <b>3 десерта</b> или подарка!</p>

    <p>После розыгрыша, мне придет уведомление о том, что вы выиграли в лотерее и я постараюсь связаться с вами как можно скорее.</p>

    <p>У вас есть <b>5 попыток</b> каждый день и достаточно неплохие шансы на победу!</p>

    {
      slots.slots.map((slot, index) => (
        <div className={classes.Rules} key={slot.id}>
          <div className={classes.RuleIcon}>
            <img src={slot.icon} className={classes.RulePicture}/>
            <img src={slot.icon} className={classes.RulePicture}/>
            <img src={slot.icon} className={classes.RulePicture}/>
          </div>
          <div className={classes.RuleName}>
            { slot.name }
          </div>
          <div className={classes.RuleDescription}>
            { slot.description }
          </div>
        </div>
      ))
    }

    <div className={classes.Rules}>
      <div className={classes.RuleIcon}>
        <Coin small={true} />
      </div>
      <div className={classes.RuleName}>
        Попытки
      </div>
      <div className={classes.RuleDescription}>
        Если напал азарт, можно взять ещё немного попыток за 2 голоса или подождать день и попробовать снова!
      </div>
    </div>

    <div className={classes.Rules}>
      <div className={classes.RuleIcon}>
        <FaInfoCircle/>
      </div>
      <div className={classes.RuleName}>
        Информация
      </div>
      <div className={classes.RuleDescription}>
        Чтобы заново открыть это окно нажмите на значок i
      </div>
    </div>
  </div>;
};

const mapStateToProps = (store) => {
  return {
    slots: store.slots,
  }
};

export default connect(
  mapStateToProps,
)(Rules)
