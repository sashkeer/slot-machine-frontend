import classes from './Machine.css';

const machine = ({ children }) => {
  return (
    <div className={classes.Wrapper}>
      {/*<div className={classes.Logo}>Чайная закусь</div>*/}
      <div className={classes.Machine}>
        <div className={classes.MachineInner}>
          {children}
        </div>
      </div>
    </div>
  );
};

export default machine;