import { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import classes from './App.css';
import Machine from '../components/Machine/Machine';
import Slot from '../components/Slot/Slot';
import Coin from '../components/Coin/Coin';
import Modal from '../components/Modal/Modal';
import Rules from '../components/Rules/Rules';
import Button from '../components/Button/Button';
import { handleLogin, handleUserPoints } from '../actions/UserActions';
import { handleNewGame } from '../actions/GameActions';
import { FaCoins, FaInfoCircle } from "react-icons/fa";
import { handleSlots } from '../actions/SlotActions';
import { handleRefill } from '../actions/RefillActions';
import { getRandomInt } from '../utils/get-random-int';
import { handleCloseModal, handleOpenModal } from '../actions/ModalActions';

const App = ({
               handleUserPointsAction,
               handleSlotsAction,
               handleLoginAction,
               handleGameAction,
               handleRefillAction,

               handleCloseModalAction,
               handleOpenModalAction,

               user,
               game,
               slots,
}) => {

  // const winningIndexes = [getRandomInt(0, 8), getRandomInt(0, 8), getRandomInt(0, 8)];
  const items = slots.slots; // TODo replace with objects
  const minAngle = 1800;
  const stepAngle = 360 / items.length;
  const itemAngles = items.map((value, index) => minAngle / items.length * index);

  useEffect(() => {
    if (!game.isFetching && !firstStart) {

      const gameIndexes = game.game.map((id) => slots.slots.findIndex((slot) => slot.id === id));
      setAnglesState(gameIndexes.map(
        (newWinnerIndex, index) => (newWinnerIndex - winningIndexes[index]) * stepAngle + minAngle + getRandomInt(0, 10) * 360),
      );
      setWinningIndexesState(gameIndexes);
      setCounterState(counter + 1);
    }
    setFirstStartState(false);
  }, [game]);

  useEffect(() => {
    handleLoginAction();
    handleSlotsAction();
    handleUserPointsAction();
  }, []);

  /*
  * State
  * */
  const [counter, setCounterState] = useState(0);
  const [firstStart, setFirstStartState] = useState(true);
  const [score, setScoreState] = useState(`00${user.points}`);
  const [angles, setAnglesState] = useState([0, 0, 0]);
  const [winningIndexes, setWinningIndexesState] = useState([0, 0, 0]);
  const [running, setRunningState] = useState(false);

  return (
    <div className={classes.App}>
      <Machine>
        { angles.map((angle, index) =>  <Slot key={index} highlight={game.isWin && slots.slotCounter === 0} newAngle={angle} counter={counter}/>) }
      </Machine>
      <div className={classes.GameNumber}>
        { game.id ? `Игра №${game.id}` : 'Нажмите на СТАРТ'}
        { game.isWin && slots.slotCounter === 0 ? 'Ура вы победили! НО пока что лотерея работает в тестовом режиме!' : '' }
      </div>
      <div className={classes.Controls}>
        <div className={classes.User} onClick={() => handleOpenModalAction()}>
          <FaInfoCircle/>
        </div>
        <div className={classes.Score}>
          {/*<div className={classes.ScoreItem}>*/}
            {/*{ slots.slotCounter }*/}
          {/*</div>*/}
          {/*<div className={classes.ScoreItem}>*/}
            {/*{ score[1] }*/}
          {/*</div>*/}
          <div className={classes.ScoreItem}>
            { user.points }
          </div>
          <Coin click={() => handleRefillAction()} />
          {/*<div className={classes.ScoreItemAdd} onClick={() => handleRefillAction()}>*/}
            {/*/!*<FaCoins/>*!/*/}
          {/*</div>*/}
        </div>
        {/*<Button click={() => startHandler([3, 2, 1])} disabled={running}>Старт</Button>*/}
        <Button click={() => handleGameAction()} disabled={slots.slotCounter > 0 || user.points <= 0 || game.isFetching}>Старт</Button>
      </div>

      <Modal>
        <Rules />
        <br/>
        <Button small={true} click={() => handleCloseModalAction()}>Понятно</Button>
      </Modal>

    </div>
  );
}

const mapStateToProps = (store) => {
  return {
    user: store.user,
    page: store.page,
    game: store.game,
    slots: store.slots,
    modal: store.modal,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    // getPhotosAction: (year) => dispatch(getPhotos(year)),
    // "приклеили" в this.props.handleLoginAction функцию, которая умеет диспатчить handleLogin
    handleLoginAction: () => dispatch(handleLogin()),
    handleGameAction: () => dispatch(handleNewGame()),
    handleRefillAction: () => dispatch(handleRefill()),
    handleSlotsAction: () => dispatch(handleSlots()),
    handleUserPointsAction: () => dispatch(handleUserPoints()),
    handleCloseModalAction: () => dispatch(handleCloseModal()),
    handleOpenModalAction: () => dispatch(handleOpenModal()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
