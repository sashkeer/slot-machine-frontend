import {
  SLOT_DONE,
  SLOT_FAIL,
  SLOT_REQUEST, SLOT_STARTED,
  SLOT_SUCCESS,
} from '../actions/SlotActions';

const initialState = {
  slots: [],
  slotCounter: 0,
  error: '',
  isFetching: false,
};

export function slotReducer(state = initialState, action) {
  switch (action.type) {

    case SLOT_DONE:
      return { ...state, slotCounter: state.slotCounter - 1 };

    case SLOT_STARTED:
      return { ...state, slotCounter: state.slotCounter + 1 };

    case SLOT_REQUEST:
      return { ...state, isFetching: true, error: '' };

    case SLOT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        slots: action.payload,
      };

    case SLOT_FAIL:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message,
      };

    default:
      return state
  }
}