import { combineReducers } from 'redux';
import { userReducer } from './UserReducers';
import { gameReducer } from './GameReducers';
import { slotReducer } from './SlotReducers';
import { modalReducer } from './ModalReducers';

export default combineReducers({
  user: userReducer,
  game: gameReducer,
  slots: slotReducer,
  modal: modalReducer,
});