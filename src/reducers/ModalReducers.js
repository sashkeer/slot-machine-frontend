import { CLOSE_MODAL, OPEN_MODAL } from '../actions/ModalActions';

const MODAL_CONFIRMED_KEY = 'MODAL.CONFIRMED';

const modalConfirmed = !!localStorage.getItem(MODAL_CONFIRMED_KEY);

const initialState = {
  isOpen: !modalConfirmed,
};

export function modalReducer(state = initialState, action) {
  switch (action.type) {

    case OPEN_MODAL:
      return { ...state, isOpen: true };

    case CLOSE_MODAL:
      localStorage.setItem(MODAL_CONFIRMED_KEY, '1');
      return { ...state, isOpen: false };

    default:
      return state
  }
}