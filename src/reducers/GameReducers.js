import {
  GAME_REQUEST,
  GAME_SUCCESS,
  GAME_FAIL,
} from '../actions/GameActions';

const initialState = {
  id: null,
  isWin: false,
  game: [0, 0, 0],
  error: '',
  isFetching: false,
};

export function gameReducer(state = initialState, action) {
  switch (action.type) {

    case GAME_REQUEST:
      return { ...state, isFetching: true, error: '' };

    case GAME_SUCCESS:
      return {
        ...state,
        isFetching: false,
        game: action.payload.game,
        isWin: action.payload.isWin,
        id: action.payload.id,
      };

    case GAME_FAIL:
      return {
        ...state,
        isFetching: false,
        error: action.payload.message,
      };

    default:
      return state
  }
}