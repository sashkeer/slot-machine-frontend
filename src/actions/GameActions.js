
import axios from 'axios';
import { handleUserPoints } from './UserActions';

export const GAME_REQUEST = 'GAME_REQUEST';
export const GAME_SUCCESS = 'GAME_SUCCESS';
export const GAME_FAIL = 'GAME_FAIL';

export function handleNewGame() {
  return function (dispatch) {
    dispatch({
      type: GAME_REQUEST,
    });

    const urlParams = new URLSearchParams(window.location.search);

    return axios.get(`/api/game/${urlParams.get('viewer_id') || 0}`)
      .then((response) => {
        dispatch({
            type: GAME_SUCCESS,
            payload: response.data,
          });

        dispatch(handleUserPoints());
      })
      .catch(() => {
        dispatch({
          type: GAME_FAIL,
          error: true,
          payload: new Error('Ошибка создания новой игры'),
        })
      });
  }
}