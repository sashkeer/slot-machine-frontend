export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export function handleOpenModal() {
  return {
    type: OPEN_MODAL,
  };
}

export function handleCloseModal() {
  return {
    type: CLOSE_MODAL,
  };
}