
import axios from 'axios';

export const SLOT_DONE = 'SLOT_DONE';
export const SLOT_STARTED = 'SLOT_STARTED';
export const SLOT_REQUEST = 'SLOT_REQUEST';
export const SLOT_SUCCESS = 'SLOT_SUCCESS';
export const SLOT_FAIL = 'SLOT_FAIL';

export function handleSlotDone () {
  return {
    type: SLOT_DONE,
  }
}

export function handleSlotStarted () {
  return {
    type: SLOT_STARTED,
  }
}

export function handleSlots() {
  return function (dispatch) {
    dispatch({
      type: SLOT_REQUEST,
    });

    return axios.get(`/api/game/slots`)
      .then((response) => {
      dispatch({
          type: SLOT_SUCCESS,
          payload: response.data,
        })
      })
      .catch(() => {
        dispatch({
          type: SLOT_FAIL,
          error: true,
          payload: new Error('Ошибка создания новой игры'),
        })
      });
  }
}