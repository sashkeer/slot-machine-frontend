import { handleUserPoints } from './UserActions';

export const REFILL_REQUEST = 'REFILL_REQUEST';

export function handleRefill() {

  return function (dispatch) {
    dispatch({
      type: REFILL_REQUEST,
    });

    const params = {
      type: 'votes',
      votes: 2
    };

    /*eslint-disable no-undef*/
    VK.callMethod('showOrderBox', params);

    VK.addCallback('onOrderSuccess', function(orderId) {
      dispatch(handleUserPoints());
    });

    VK.addCallback('onOrderFail', function(errorCode) {
      console.log('Order fail', errorCode);
    });

    VK.addCallback('onOrderCancel', function() {
      console.log('Order cancel');
    });

  }
}