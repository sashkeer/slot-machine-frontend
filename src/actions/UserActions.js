import { GAME_FAIL, GAME_SUCCESS } from './GameActions';
import axios from 'axios/index';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const POINTS_REQUEST = 'POINTS_REQUEST';
export const POINTS_SUCCESS = 'POINTS_SUCCESS';
export const POINTS_FAIL = 'POINTS_FAIL';

export function handleLogin() {
  return function (dispatch) {
    dispatch({
      type: LOGIN_REQUEST,
    });

    const urlParams = new URLSearchParams(window.location.search);

    //eslint-disable-next-line no-undef
    return VK.api('users.get', { user_ids: urlParams.get('viewer_id') }, function (data) {
      const user = data.response.pop();
      if (user) {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: `${user.first_name} ${user.last_name}`,
        })
      } else {
        dispatch({
          type: LOGIN_FAIL,
          error: true,
          payload: new Error('Ошибка авторизации'),
        })

      }
    });
  }
}

export function handleUserPoints() {
  return function (dispatch) {
    dispatch({
      type: POINTS_REQUEST,
    });

    const urlParams = new URLSearchParams(window.location.search);

    return axios.get(`/api/game/points/${urlParams.get('viewer_id') || 0}`)
    .then((response) => {
      dispatch({
        type: POINTS_SUCCESS,
        payload: response.data,
      })
    })
    .catch(() => {
      dispatch({
        type: POINTS_FAIL,
        error: true,
        payload: new Error('Ошибка пользовательских очков'),
      })
    });
  }
}