module.exports = {
    apps : [],
    deploy : {
        production : {
            user : 'root',
            host : '178.128.205.255',
            ref  : 'origin/master',
            repo : 'git@gitlab.com:sashkeer/slot-machine-frontend.git',
            path : '/var/www/slot-machine-frontend',
            'post-deploy' : 'npm install && npm run build'
        }
    }
};
